package dam.android.fabricio.u3t4event;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ResourceBundle;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private TextView tvEventName;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btAccept;
    private Button btCancel;
    private String priority = "Normal";
    private String safeTvCurrentData;
    private String safeTvetPlace;
    private String[] month;
    private EditText etPlace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);
        setUI();
        Bundle inputData = getIntent().getExtras();
        tvEventName.setText(inputData.getString("EventName"));
        etPlace.setText(inputData.getString("etPlace"));

        safeTvCurrentData = inputData.getString("tvCurrentData");
        safeTvetPlace =  inputData.getString("etPlace");
        Resources res = getResources();

        month = res.getStringArray(R.array.name_month);

    }

    private void setUI() {
        tvEventName = (TextView) findViewById(R.id.tvEventName);
        rgPriority = (RadioGroup) findViewById(R.id.rgPriority);
        dpDate = (DatePicker) findViewById(R.id.dpDate);
        tpTime = (TimePicker) findViewById(R.id.tpTime);
        etPlace =  (EditText)findViewById(R.id.etPlace);

        tpTime.setIs24HourView(true);
        btAccept = (Button) findViewById(R.id.btAccept);
        btCancel = (Button) findViewById(R.id.btCancel);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
        rgPriority.check(R.id.rbNormal);

    }

    @Override
    public void onClick(View v) {
        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();
        switch (v.getId()) {
            case R.id.btAccept:
                eventData.putString("EventData", "Place: "+etPlace.getText()+"\n"+"Priority: " + priority + "\n" + "Month: " + month[dpDate.getMonth()] + "\n" + "Day: " + dpDate.getDayOfMonth() + "\n" + "Year: " + dpDate.getYear()+"\n"+"HOUR: "+tpTime.getHour()+":"+tpTime.getMinute());
                eventData.putString("etPlace",safeTvetPlace);
                break;
            case R.id.btCancel:
               eventData.putString("EventData",safeTvCurrentData);
                eventData.putString("etPlace",safeTvetPlace);
                break;
        }
        activityResult.putExtras(eventData);
        setResult(RESULT_OK,activityResult);
        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rbLow:
                priority = "Low";
                break;
            case R.id.rbNormal:
                priority = "Normal";
                break;
            case R.id.rbHigh:
                priority = "High";
                break;
        }
    }
}